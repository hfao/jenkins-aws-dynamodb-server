package cn.fabrice.jenkins.controller;

import cn.fabrice.jenkins.module.AwsLog;
import cn.fabrice.jenkins.module.JenkinsLog;
import cn.fabrice.jenkins.pojo.DataResult;
import cn.fabrice.jenkins.pojo.RequestParams;
import cn.fabrice.jenkins.service.AwsDynamoDbService;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;
import com.jfinal.kit.StrKit;

import java.util.List;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 07:33
 * @description
 */
public class CoreController extends Controller {
    @Inject
    private AwsDynamoDbService awsDynamoDbService;

    /**
     * collect log
     *
     * @param payload
     */
    public void collect(String payload) {
        //for test
        if (StrKit.isBlank(payload)) {
            renderText("success");
            return;
        }
        //collect data formally
        JenkinsLog jenkinsLog = JSONObject.parseObject(payload, JenkinsLog.class);
        awsDynamoDbService.putData(jenkinsLog);
        renderText("success");
    }

    /**
     * for search
     *
     * @param params
     */
    public void search(@Para("") RequestParams params) {
        List<AwsLog> logList = awsDynamoDbService.list(params.getJobName(), params.getContent(),
                params.getBeginDateTime(), params.getEndDateTime());
        renderJson(DataResult.data(logList));
    }
}
