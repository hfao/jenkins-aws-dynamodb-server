package cn.fabrice.jenkins.controller;

import cn.fabrice.jenkins.dao.DynamoDbDao;
import cn.fabrice.jenkins.pojo.BaseResult;
import cn.fabrice.jenkins.pojo.DataResult;
import cn.fabrice.jenkins.service.AwsDynamoDbService;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 11:32
 * @description
 */
public class ConfigureController extends Controller {
    @Inject
    private AwsDynamoDbService awsDynamoDbService;

    /**
     * get configure
     */
    public void get() {
        Map<String, String> resMap = new HashMap<>(4);
        resMap.put("appId", DynamoDbDao.getInstance().getAppId());
        resMap.put("appSecret", DynamoDbDao.getInstance().getAppSecret());
        resMap.put("region", DynamoDbDao.getInstance().getRegionStr());
        resMap.put("tableName", DynamoDbDao.getInstance().getTableName());
        renderJson(DataResult.data(resMap));
    }

    /**
     * set configure
     *
     * @param appId
     * @param appSecret
     * @param region
     * @param tableName
     */
    public void set(String appId, String appSecret, String region, String tableName) {
        if (DynamoDbDao.getInstance().reset(appId, appSecret, region, tableName)) {
            String path = "base_config.properties";
            Prop prop = PropKit.use(path);
            prop.getProperties().setProperty("AWS_APP_ID", appId);
            prop.getProperties().setProperty("AWS_APP_SECRET", appSecret);
            prop.getProperties().setProperty("AWS_REGION", region);
            prop.getProperties().setProperty("TABLE_NAME", tableName);
            //the collection info is right
            if (!awsDynamoDbService.tableIsExist(DynamoDbDao.getInstance().getTableName())) {
                //the table not exist, need to create
                awsDynamoDbService.createTable(DynamoDbDao.getInstance().getTableName());
            }
            try (FileOutputStream oFile = new FileOutputStream(path)) {
                //将Properties中的属性列表（键和元素对）写入输出流
                prop.getProperties().store(oFile, "");
                renderJson(BaseResult.ok());
            } catch (IOException e) {
                renderJson(BaseResult.fail(e.getLocalizedMessage()));
            }
            return;
        }
        renderJson(BaseResult.fail("连接信息错误，重置为原始值"));
    }
}
