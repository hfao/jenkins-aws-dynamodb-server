package cn.fabrice.jenkins;

import cn.fabrice.jenkins.controller.ConfigureController;
import cn.fabrice.jenkins.controller.CoreController;
import cn.fabrice.jenkins.dao.DynamoDbDao;
import cn.fabrice.jenkins.service.AwsDynamoDbService;
import cn.fabrice.jfinal.ext.cros.interceptor.CrossInterceptor;
import com.jfinal.aop.Aop;
import com.jfinal.config.*;
import com.jfinal.kit.PropKit;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

/**
 * @author fenghao
 * @email fh941005@163.com
 * @date 2020-03-15 10:30
 * @description application runner
 */
public class App extends JFinalConfig {
    @Override
    public void configConstant(Constants me) {
        PropKit.use("base_config.properties");
        //设置开发模式
        me.setDevMode(PropKit.getBoolean("devMode"));
        //设置允许AOP注入
        me.setInjectDependency(true);
    }

    @Override
    public void configRoute(Routes me) {
        me.add("/", CoreController.class);
        me.add("/configure", ConfigureController.class);
    }

    @Override
    public void configEngine(Engine me) {
    }

    @Override
    public void configPlugin(Plugins me) {

    }

    @Override
    public void configInterceptor(Interceptors me) {
        me.add(new CrossInterceptor());
    }

    @Override
    public void configHandler(Handlers me) {

    }

    @Override
    public void onStart() {
        System.out.println("app starting...");
        AwsDynamoDbService awsDynamoDbService = Aop.get(AwsDynamoDbService.class);
        DynamoDbDao dao = DynamoDbDao.getInstance();
        if (!awsDynamoDbService.tableIsExist(dao.getTableName())) {
            //the table not exist, need to create
            awsDynamoDbService.createTable(dao.getTableName());
        }
    }

    @Override
    public void onStop() {
        System.out.println("app stopping...");
    }

    public static void main(String[] args) throws Exception {
        UndertowServer.start(App.class);
    }
}
