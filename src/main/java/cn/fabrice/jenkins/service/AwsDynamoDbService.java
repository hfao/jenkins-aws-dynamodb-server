package cn.fabrice.jenkins.service;

import cn.fabrice.jenkins.dao.DynamoDbDao;
import cn.fabrice.jenkins.module.AwsLog;
import cn.fabrice.jenkins.module.JenkinsLog;
import cn.fabrice.kit.Kits;
import com.jfinal.kit.StrKit;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-19 14:54
 * @description
 */
public class AwsDynamoDbService {
    private DynamoDbClient ddb;

    public AwsDynamoDbService() {
        ddb = DynamoDbDao.getInstance().getDdb();
    }

    /**
     * list the aws dynamodb table
     *
     * @return
     */
    public List<String> listAllTable() {
        List<String> tableNameList = new ArrayList<>();
        boolean moreTables = true;
        String lastName = null;
        while (moreTables) {
            try {
                ListTablesResponse response = null;
                if (lastName == null) {
                    ListTablesRequest request = ListTablesRequest.builder().build();
                    response = ddb.listTables(request);
                } else {
                    ListTablesRequest request = ListTablesRequest.builder()
                            .exclusiveStartTableName(lastName).build();
                    response = ddb.listTables(request);
                }

                tableNameList.addAll(response.tableNames());

                lastName = response.lastEvaluatedTableName();
                if (lastName == null) {
                    moreTables = false;
                }
            } catch (DynamoDbException e) {
                break;
            }
        }
        return tableNameList;
    }

    /**
     * judge the table is exist or not
     *
     * @param tableName
     * @return
     */
    public boolean tableIsExist(String tableName) {
        return listAllTable().contains(tableName);
    }

    /**
     * create table in aws dynamodb
     *
     * @param tableName
     * @return
     */
    public boolean createTable(String tableName) {
        CreateTableRequest request = CreateTableRequest.builder()
                .attributeDefinitions(AttributeDefinition.builder()
                                .attributeName("jobName")
                                .attributeType(ScalarAttributeType.S)
                                .build(),
                        AttributeDefinition.builder()
                                .attributeName("buildNumber")
                                .attributeType(ScalarAttributeType.N)
                                .build()
                )
                .keySchema(KeySchemaElement.builder()
                                .attributeName("jobName")
                                .keyType(KeyType.HASH)
                                .build(),
                        KeySchemaElement.builder()
                                .attributeName("buildNumber")
                                .keyType(KeyType.RANGE)
                                .build())
                .provisionedThroughput(ProvisionedThroughput.builder()
                        .readCapacityUnits(10L)
                        .writeCapacityUnits(10L)
                        .build())
                .tableName(tableName)
                .build();

        try {
            ddb.createTable(request);
            return true;
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            return true;
        }
    }

    /**
     * put data into aws dynamodb
     *
     * @param jenkinsLog
     * @return
     */
    public boolean putData(JenkinsLog jenkinsLog) {
        //judge data is exist
        Map<String, AttributeValue> map = getByKey(jenkinsLog.getData().getProjectName(), jenkinsLog.getData().getId());
        if (map != null && !map.keySet().isEmpty()) {
            //just update console log
            String consoleLog = map.get("consoleLog").s();
            consoleLog += "\n" + String.join("\n", jenkinsLog.getMessage());
            return updateData(jenkinsLog.getData().getProjectName(), jenkinsLog.getData().getId(), consoleLog);
        }
        HashMap<String, AttributeValue> itemValues = new HashMap<>(4);

        // Add content to the table
        itemValues.put("jobName", AttributeValue.builder().s(jenkinsLog.getData().getProjectName()).build());
        itemValues.put("buildNumber", AttributeValue.builder().n(jenkinsLog.getData().getId()).build());
        itemValues.put("consoleLog", AttributeValue.builder().s(String.join("\n", jenkinsLog.getMessage())).build());
        itemValues.put("buildTime", AttributeValue.builder().s(jenkinsLog.getBuildTimestamp()).build());

        // Create a PutItemRequest object
        PutItemRequest request = PutItemRequest.builder()
                .tableName(DynamoDbDao.getInstance().getTableName())
                .item(itemValues)
                .build();
        try {
            ddb.putItem(request);
            return true;
        } catch (DynamoDbException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * get the item by key
     * when stream data coming, add info to exist one
     *
     * @param jobName
     * @param buildNumber
     * @return
     */
    public Map<String, AttributeValue> getByKey(String jobName, String buildNumber) {
        HashMap<String, AttributeValue> keyToGet = new HashMap<String, AttributeValue>();

        keyToGet.put("jobName", AttributeValue.builder()
                .s(jobName).build());
        keyToGet.put("buildNumber", AttributeValue.builder().n(buildNumber).build());

        // Create a GetItemRequest object
        GetItemRequest request = GetItemRequest.builder()
                .key(keyToGet)
                .tableName(DynamoDbDao.getInstance().getTableName())
                .build();

        try {
            return ddb.getItem(request).item();
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public boolean updateData(String jobName, String buildNumber, String consoleLog) {
        HashMap<String, AttributeValue> itemKey = new HashMap<>(2);

        itemKey.put("jobName", AttributeValue.builder().s(jobName).build());
        itemKey.put("buildNumber", AttributeValue.builder().n(buildNumber).build());

        HashMap<String, AttributeValueUpdate> updatedValues =
                new HashMap<>(1);

        // Update the column specified by name with updatedVal
        updatedValues.put("consoleLog", AttributeValueUpdate.builder()
                .value(AttributeValue.builder().s(consoleLog).build())
                .action(AttributeAction.PUT)
                .build());

        UpdateItemRequest request = UpdateItemRequest.builder()
                .tableName(DynamoDbDao.getInstance().getTableName())
                .key(itemKey)
                .attributeUpdates(updatedValues)
                .build();

        try {
            ddb.updateItem(request);
            return true;
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

    /**
     * list the item of dynamodb table
     *
     * @param jobName
     * @param content
     * @param beginDateTime
     * @param endDateTime
     * @return
     */
    public List<AwsLog> list(String jobName, String content, String beginDateTime, String endDateTime) {
        String filterConditionExpress = "";
        //set up mapping of the partition name with the value
        HashMap<String, AttributeValue> attrValues = new HashMap<>(4);
        boolean isFirst = true;
        if (StrKit.notBlank(jobName)) {
            filterConditionExpress = "jobName = :name";
            isFirst = false;
            attrValues.put(":name", AttributeValue.builder().s(jobName).build());
        }

        if (StrKit.notBlank(beginDateTime)) {
            if (isFirst) {
                filterConditionExpress = "buildTime >= :beginTime";
                isFirst = false;
            } else {
                filterConditionExpress += " and buildTime >= :beginTime";
            }

            attrValues.put(":beginTime", AttributeValue.builder().s(beginDateTime).build());
        }

        if (StrKit.notBlank(endDateTime)) {
            if (isFirst) {
                filterConditionExpress = "buildTime <= :endTime";
                isFirst = false;
            } else {
                filterConditionExpress += " and buildTime <= :endTime";
            }
            attrValues.put(":endTime", AttributeValue.builder().s(endDateTime).build());
        }

        if (StrKit.notBlank(content)) {
            if (isFirst) {
                filterConditionExpress = "contains(consoleLog, :log)";
            } else {
                filterConditionExpress += " and contains(consoleLog, :log)";
            }
            attrValues.put(":log", AttributeValue.builder().s(content).build());
        }
        ScanRequest.Builder builder = ScanRequest.builder()
                .tableName(DynamoDbDao.getInstance().getTableName());
        if (StrKit.notBlank(filterConditionExpress)) {
            builder.filterExpression(filterConditionExpress);
            builder.expressionAttributeValues(attrValues);
        }
        // Cretae a QueryRequest object
        ScanRequest scanRequest = builder.build();

        List<AwsLog> logList = new ArrayList<>();
        try {
            ScanResponse response = ddb.scan(scanRequest);
            response.items().forEach(item -> {
                AwsLog awsLog = new AwsLog();
                awsLog.setId(Kits.getUuid());
                awsLog.setJobName(item.get("jobName").s());
                awsLog.setBuildNumber(item.get("buildNumber").n());
                awsLog.setBuildTime(item.get("buildTime").s());
                awsLog.setConsoleLog(item.get("consoleLog").s());
                logList.add(awsLog);
            });
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
        }
        return logList;
    }
}
