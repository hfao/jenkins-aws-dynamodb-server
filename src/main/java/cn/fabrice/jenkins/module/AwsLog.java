package cn.fabrice.jenkins.module;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 19:48
 * @description
 */
public class AwsLog {
    private String id;
    private String jobName;
    private String buildNumber;
    private String buildTime;
    private String consoleLog;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }

    public String getConsoleLog() {
        return consoleLog;
    }

    public void setConsoleLog(String consoleLog) {
        this.consoleLog = consoleLog;
    }
}
