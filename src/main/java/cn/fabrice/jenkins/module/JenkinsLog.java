package cn.fabrice.jenkins.module;

import java.util.List;

/**
 * @author fenghao
 * @email fh941005@163.com
 * @date 2020-03-16 15:22
 * @description
 */

public class JenkinsLog {

    /**
     * data : {"id":"19","projectName":"test3","fullProjectName":"test3","displayName":"#19","fullDisplayName":"test3 #19","url":"job/test3/19/","buildHost":"Jenkins","buildLabel":"master","buildNum":19,"buildDuration":238,"rootProjectName":"test3","rootFullProjectName":"test3","rootProjectDisplayName":"#19","rootBuildNum":19}
     * message : ["Running on Jenkins in /Users/fenghao/Documents/Projects/JavaProjects/logstash-plugin/work/workspace/test3"]
     * source : jenkins
     * sourceHost : http://localhost:7080/jenkins/
     * buildTimestamp : 2020-03-21 09:26:34
     * timestamp : 2020-03-21 09:26:34
     */

    private DataBean data;
    private String source;
    private String sourceHost;
    private String buildTimestamp;
    private String timestamp;
    private List<String> message;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceHost() {
        return sourceHost;
    }

    public void setSourceHost(String sourceHost) {
        this.sourceHost = sourceHost;
    }

    public String getBuildTimestamp() {
        return buildTimestamp;
    }

    public void setBuildTimestamp(String buildTimestamp) {
        this.buildTimestamp = buildTimestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public static class DataBean {
        /**
         * id : 19
         * projectName : test3
         * fullProjectName : test3
         * displayName : #19
         * fullDisplayName : test3 #19
         * url : job/test3/19/
         * buildHost : Jenkins
         * buildLabel : master
         * buildNum : 19
         * buildDuration : 238
         * rootProjectName : test3
         * rootFullProjectName : test3
         * rootProjectDisplayName : #19
         * rootBuildNum : 19
         */

        private String id;
        private String projectName;
        private String fullProjectName;
        private String displayName;
        private String fullDisplayName;
        private String url;
        private String buildHost;
        private String buildLabel;
        private int buildNum;
        private int buildDuration;
        private String rootProjectName;
        private String rootFullProjectName;
        private String rootProjectDisplayName;
        private int rootBuildNum;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getFullProjectName() {
            return fullProjectName;
        }

        public void setFullProjectName(String fullProjectName) {
            this.fullProjectName = fullProjectName;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getFullDisplayName() {
            return fullDisplayName;
        }

        public void setFullDisplayName(String fullDisplayName) {
            this.fullDisplayName = fullDisplayName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getBuildHost() {
            return buildHost;
        }

        public void setBuildHost(String buildHost) {
            this.buildHost = buildHost;
        }

        public String getBuildLabel() {
            return buildLabel;
        }

        public void setBuildLabel(String buildLabel) {
            this.buildLabel = buildLabel;
        }

        public int getBuildNum() {
            return buildNum;
        }

        public void setBuildNum(int buildNum) {
            this.buildNum = buildNum;
        }

        public int getBuildDuration() {
            return buildDuration;
        }

        public void setBuildDuration(int buildDuration) {
            this.buildDuration = buildDuration;
        }

        public String getRootProjectName() {
            return rootProjectName;
        }

        public void setRootProjectName(String rootProjectName) {
            this.rootProjectName = rootProjectName;
        }

        public String getRootFullProjectName() {
            return rootFullProjectName;
        }

        public void setRootFullProjectName(String rootFullProjectName) {
            this.rootFullProjectName = rootFullProjectName;
        }

        public String getRootProjectDisplayName() {
            return rootProjectDisplayName;
        }

        public void setRootProjectDisplayName(String rootProjectDisplayName) {
            this.rootProjectDisplayName = rootProjectDisplayName;
        }

        public int getRootBuildNum() {
            return rootBuildNum;
        }

        public void setRootBuildNum(int rootBuildNum) {
            this.rootBuildNum = rootBuildNum;
        }
    }
}
