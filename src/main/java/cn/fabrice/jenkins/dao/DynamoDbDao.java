package cn.fabrice.jenkins.dao;

import com.jfinal.kit.PropKit;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.ListTablesRequest;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 09:30
 * @description
 */
public class DynamoDbDao {
    private static DynamoDbDao dao;
    private String appId;
    private String appSecret;
    private String regionStr;
    private String tableName;
    private DynamoDbClient ddb;

    private DynamoDbDao() {

    }

    private boolean init() {
        try {
            this.appId = PropKit.get("AWS_APP_ID");
            this.appSecret = PropKit.get("AWS_APP_SECRET");
            this.regionStr = PropKit.get("AWS_REGION");
            this.tableName = PropKit.get("TABLE_NAME");
            AwsBasicCredentials credentials = AwsBasicCredentials.create(appId, appSecret);
            Region region = Region.of(regionStr);
            ddb = DynamoDbClient.builder().region(region).credentialsProvider(StaticCredentialsProvider.create(credentials)).build();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void close() {
        if (ddb != null) {
            ddb.close();
        }
    }

    public boolean reset(String appId, String appSecret, String regionStr, String tableName) {
        close();
        try {
            AwsBasicCredentials credentials = AwsBasicCredentials.create(appId, appSecret);
            Region region = Region.of(regionStr);
            System.out.println(appId);
            System.out.println(appSecret);
            ddb = DynamoDbClient.builder().region(region).credentialsProvider(StaticCredentialsProvider.create(credentials)).build();
            ListTablesRequest request = ListTablesRequest.builder().build();
            ddb.listTables(request);
            this.appId = appId;
            this.appSecret = appSecret;
            this.regionStr = regionStr;
            this.tableName = tableName;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static DynamoDbDao getInstance() {
        if (dao == null) {
            dao = new DynamoDbDao();
            dao.init();
        }
        return dao;
    }

    public String getAppId() {
        return appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public String getRegionStr() {
        return regionStr;
    }

    public String getTableName() {
        return tableName;
    }

    public DynamoDbClient getDdb() {
        return ddb;
    }
}
