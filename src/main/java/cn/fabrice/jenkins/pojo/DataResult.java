package cn.fabrice.jenkins.pojo;

/**
 * @author fye
 * @team occam
 * @email fh941005@163.com
 * @date 2019-01-12 10:42
 * @description 包含结果的返回值实体类
 */
public class DataResult extends BaseResult {
    /**
     * 返回结果值
     */
    private Object data;

    /**
     * 基本构造方法
     */
    public DataResult() {
        super();
    }

    /**
     * 简化操作返回值
     *
     * @param data 返回值
     * @return 操作结果实体类-成功
     */
    public static DataResult data(Object data) {
        return new DataResult(BaseResultConstants.OK, data);
    }

    /**
     * 简化操作返回值
     *
     * @param code 返回码
     * @param data 返回值
     * @return 操作结果实体类-成功
     */
    public static DataResult res(int code, Object data) {
        return new DataResult(code, data);
    }

    /**
     * 简化操作返回值
     *
     * @param code 返回码
     * @param msg  返回码说明
     * @param data 返回值
     * @return 操作结果实体类-成功
     */
    public static DataResult res(int code, String msg, Object data) {
        return new DataResult(code, msg, data);
    }

    /**
     * 只有返回码的构造方法
     *
     * @param code 返回码
     * @param data 返回值
     */
    public DataResult(int code, Object data) {
        super(code);
        this.data = data;
    }

    /**
     * 包含返回码和返回说明的构造方法
     *
     * @param code 返回码
     * @param msg  返回说明
     * @param data 返回值
     */
    public DataResult(int code, String msg, Object data) {
        super(code, msg);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
