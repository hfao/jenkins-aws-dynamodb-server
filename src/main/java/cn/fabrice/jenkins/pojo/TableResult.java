package cn.fabrice.jenkins.pojo;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2019-07-04 14:54
 * @description 表格数据结果类
 */
public class TableResult extends DataResult {
    /**
     * 总数据量
     */
    private int total;

    public TableResult() {
        super();
    }

    /**
     * 简化Table返回值
     *
     * @param code  返回码
     * @param total 总条数
     * @param data  返回的数据
     * @return 对应实体类
     */
    public static TableResult set(int code, int total, Object data) {
        return new TableResult(code, data, total);
    }

    /**
     * 已默认返回码的方式渲染数据表格结果
     *
     * @param total 数据总条数
     * @param data  具体数据值
     * @return 对应实体类
     */
    public static TableResult set(int total, Object data) {
        return new TableResult(BaseResultConstants.OK, data, total);
    }

    /**
     * 构造函数
     *
     * @param code  返回码
     * @param data  返回的数据
     * @param total 总条数
     */
    public TableResult(int code, Object data, int total) {
        super(code, data);
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
