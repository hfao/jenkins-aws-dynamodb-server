package cn.fabrice.jenkins.pojo;

/**
 * @author fye
 * @team occam
 * @email fh941005@163.com
 * @date 2019-01-12 17:34
 * @description 返回结果基类
 */
public class BaseResult implements java.io.Serializable {
    /**
     * 返回码
     */
    private int code;
    /**
     * 返回说明
     */
    private String msg;
    /**
     * 返回的时间戳
     */
    private long time;

    /**
     * 简化操作返回值
     *
     * @return 操作结果实体类-成功
     */
    public static BaseResult ok() {
        return new BaseResult(BaseResultConstants.OK);
    }

    /**
     * 简化操作返回值
     *
     * @param msg 返回说明
     * @return 操作结果实体类-成功
     */
    public static BaseResult ok(String msg) {
        return new BaseResult(BaseResultConstants.OK, msg);
    }

    /**
     * 简化操作返回值
     *
     * @return 操作结果实体类-失败
     */
    public static BaseResult fail() {
        return new BaseResult(BaseResultConstants.FAIL);
    }

    /**
     * 简化操作返回值
     *
     * @param msg 返回说明
     * @return 操作结果实体类-失败
     */
    public static BaseResult fail(String msg) {
        return new BaseResult(BaseResultConstants.FAIL, msg);
    }

    /**
     * 简化操作返回值
     *
     * @param code 返回码
     * @return 操作结果实体类-失败
     */
    public static BaseResult res(int code) {
        return new BaseResult(code);
    }

    /**
     * 简化操作返回值
     *
     * @param code 返回码
     * @param msg  返回说明
     * @return 操作结果实体类-失败
     */
    public static BaseResult res(int code, String msg) {
        return new BaseResult(code, msg);
    }

    /**
     * 401返回
     *
     * @return 401操作结果默认实体类
     */
    public static BaseResult error401() {
        return new BaseResult(BaseResultConstants.CODE_401);
    }

    /**
     * 401返回
     *
     * @param msg 自定义返回消息
     * @return 401操作结果默认实体类
     */
    public static BaseResult error401(String msg) {
        return new BaseResult(BaseResultConstants.CODE_401, msg);
    }

    /**
     * 404返回
     *
     * @return 404操作结果默认实体类
     */
    public static BaseResult error404() {
        return new BaseResult(BaseResultConstants.CODE_404);
    }

    /**
     * 404返回
     *
     * @param msg 自定义返回消息
     * @return 404操作结果默认实体类
     */
    public static BaseResult error404(String msg) {
        return new BaseResult(BaseResultConstants.CODE_404, msg);
    }

    /**
     * 500返回
     *
     * @return 500操作结果默认实体类
     */
    public static BaseResult error500() {
        return new BaseResult(BaseResultConstants.CODE_500);
    }

    /**
     * 500返回
     *
     * @param msg 自定义返回消息
     * @return 500操作结果默认实体类
     */
    public static BaseResult error500(String msg) {
        return new BaseResult(BaseResultConstants.CODE_500, msg);
    }

    /**
     * 违法操作返回
     *
     * @return 违法操作结果默认实体类
     */
    public static BaseResult illegal() {
        return new BaseResult(BaseResultConstants.ILLEGAL_ACCESS);
    }

    /**
     * 违法操作返回
     *
     * @param msg 自定义返回消息
     * @return 违法操作结果默认实体类
     */
    public static BaseResult illegal(String msg) {
        return new BaseResult(BaseResultConstants.ILLEGAL_ACCESS, msg);
    }

    /**
     * 基本构造方法
     */
    public BaseResult() {
        super();
    }

    /**
     * 只有返回码的构造方法
     *
     * @param code 返回码
     */
    public BaseResult(int code) {
        this.code = code;
        this.msg = BaseResultConstants.getMsg(code);
        this.time = System.currentTimeMillis();
    }

    /**
     * 包含返回码和返回说明的构造方法
     *
     * @param code 返回码
     * @param msg  返回说明
     */
    public BaseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.time = System.currentTimeMillis();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
        this.msg = BaseResultConstants.getMsg(code);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    /**
     * 是否成功
     *
     * @return 成功-true/失败-false
     */
    public boolean isOk() {
        return BaseResultConstants.OK == this.code;
    }

    /**
     * 是否失败
     *
     * @return 失败-true/成功-false
     */
    public boolean isFail() {
        return BaseResultConstants.OK != this.code;
    }
}
