package cn.fabrice.jenkins.pojo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fye
 * @team occam
 * @email fh941005@163.com
 * @date 2019-01-12 10:34
 * @description 返回结果常量（其他返回值与返回说明请基于此补充）
 */
public class BaseResultConstants {
    /**
     * 非法访问
     */
    public static final int ILLEGAL_ACCESS = -2;
    /**
     * 请求成功标志
     */
    public static final int OK = 0;
    /**
     * 请求失败标志
     */
    public static final int FAIL = -1;
    /**
     * 格式错误
     */
    public static final int ERROR_FORMAT = 90000;
    /**
     * 数据已存在
     */
    public static final int CODE_DATA_IS_EXIST = -3;
    /**
     * 数据不存在
     */
    public static final int CODE_DATA_NOT_EXIST = -4;
    public static final int CODE_404 = 404;
    public static final int CODE_401 = 401;
    public static final int CODE_500 = 500;


    /**
     * 返回码对应的说明
     */
    private static Map<Integer, String> codeMsg = new HashMap<Integer, String>() {
        {
            put(ILLEGAL_ACCESS, "非法访问");
            put(OK, "ok");
            put(FAIL, "fail");
            put(ERROR_FORMAT, "格式错误");
            put(CODE_DATA_IS_EXIST, "数据已存在");
            put(CODE_DATA_NOT_EXIST, "数据不存在");
            put(CODE_401, "401");
            put(CODE_404, "404");
            put(CODE_500, "500");
        }
    };

    /**
     * 添加返回值信息
     *
     * @param code 返回码
     * @param msg  返回信息
     */
    public static void addResultInfo(int code, String msg) {
        codeMsg.put(code, msg);
    }

    /**
     * 添加返回值信息
     *
     * @param resultMap 返回数据
     */
    public static void addResultInfo(Map<Integer, String> resultMap) {
        codeMsg.putAll(resultMap);
    }


    /**
     * 获取对应操作的描述说明
     *
     * @param code 返回码
     * @return 返回码的对应说明
     */
    public static String getMsg(int code) {
        if (codeMsg.containsKey(code)) {
            return codeMsg.get(code);
        }
        return "";
    }
}
